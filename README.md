% Git
% Szymon Rogalski
% 2022-05-19

# Big Hello

hello again

# Getting started

Get started with GIT & GitLab

[this presentation](https://pirgos.gitlab.io/0_start_with_git)

# Access to Gitlab

Let's create account: 
    
[https://gitlab.com/](https://gitlab.com/)


# Installation

Git client might be helpful:

[download](https://git-scm.com/download)

# Basic setup

open command line, and set your username & email

like:
```
git config --global user.name "Szymon Rogalski"
email: git config --global user.email my.email@com
```

# Checkout code

go to directory, you want to use for git repository on your local machine, and execute in command line:
```
git clone https://gitlab.com/Pirgos/0_start_with_git.git
```

then go to directory ***0_start_with_git***, and check content

# Git status

to check what's going on, execute:
```
git status
```

# Git flow schema

![](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)


# Git branches

![](https://i0.wp.com/hexquote.com/wp-content/uploads/2020/02/Branch.jpg)

# Let's add

* let's create new branch in our repository (git branch szymon_branch)
* we want to swich on already created branch ( git checkout szymon_branch ) 
(#HINT git checkout -b szymon_branch, create and swich branch)
* create now file in repository directory (e.g. echo "here Szymon" > szymon_file.txt )
* let's add this file to staging area (git add szymon_file.txt )
* lets commit to local repository (git commit -m"new szymon_file.txt file")
* let's try to push to remote repository (git push origin szymon_branch)

# SSH key

git-bash:

```
ssh-keygen -t rsa -b 2048 -C "training_key"

cat ~/.ssh/id_rsa.pub | clip

paste in GitLab->Preferences->SSH key
```

details: https://docs.gitlab.com/ee/ssh/


# Git commands

![](https://i.redd.it/g868kpt6sax41.jpg)


# To fetch data from remote repository we use

* git fetch - Download objects and refs from another repository
* git pull - Fetch from and integrate with local branch

# Typical Scenario

* git fetch
* git checkout -b <new_branch>
* git pull
* ... code changes
* git add
* git commit 
* git push


# Aliases

* git config --global alias.co checkout
* git config --global alias.br branch
* git config --global alias.ci commit
* git config --global alias.st status

# Checking log
```
git log --all --graph

git config --global alias.logi "log --all --decorate --oneline --graph"

git logi
```

# Merging code

when we want to merge two branches, we execute:

```
git merge name_of_branch_we_want_to_merge
```

# Merging fast forward

![](https://www.bogotobogo.com/cplusplus/images/Git/Fast_Forward_Merge/FastForwardMerge.png)

# Merging fast forward - todo

* clone this project to your local machine
* create new branch 'one'
* in this branch create new file (like yourname.txt) with line of text, then add and commit 
* create from 'one' branch next branch 'two' 
* in 'two' branch add new line in file yourname.txt, add and commit
* checkout main branch
* check logs
* merge one to main (git merge one)
* check logs
* merge one to main (git merge two)
* check logs

# Deleting branches

```
git branch -d one
git branch -d two
```


# reset local repo to remote state

```
git fetch origin
git reset --hard origin/main
```

# Merging - conflicts

![](https://www.nobledesktop.com/image/gitresources/git-branches-merge.png)

# Merging Conflicts - todo

* create new branch 'one'
* in this branch create new file (like yourname.txt) with line of text, then add and commit 
* create from 'one' branch next branch 'two' 
* in 'two' branch add new line in file yourname.txt, add and commit
* checkout one branch, then in 'one' branch add new line in file yourname.txt, add and commit
* checkout main branch
* check logs
* merge one to main (git merge one)
* check logs
* merge one to main (git merge two)
* check logs



# Documentation

[https://git-scm.com/doc](https://git-scm.com/doc)

# Git commands in Easy Way

[git-commands-in-easy-way](https://www.mytechmint.com/understanding-useful-git-commands-in-easy-way/)

# Git - Courses

[percipio](https://share.percipio.com/cd/HjxEwVQmQ)

[youtube](https://www.youtube.com/watch?v=RGOj5yH7evk)





